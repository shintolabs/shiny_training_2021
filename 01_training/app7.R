

# Als app6, maar met mooie layout en vormgeving mbv navbarPage.
# - We gebruiken de bslib package om een theme te kiezen. Dit kun je ook zelf maken/aanpassen,
# maar dit is een snelle manier om strakke vormgeving toe te passen zonder veel moeite.
# - Gebruik van icon() voor icoontjes (dit helpt vormgeving en duiding meer dan je denkt)

library(shiny)
library(bslib)


ui <- navbarPage(
  
  # version is voor Bootstrap versie (3 (standaard), 4 (veel moderner), 5 (bijna idem aan 4))
  # bootswatch themes: zie bootswatch_themes() voor alle opties
  theme = bs_theme(version = 4, bootswatch = "flatly"),
  title = tagList(icon("house-damage"), "DT en ggplot"),
  
  # Eerste tab op de pagina.
  # We plaatsen een icon() tagList, wat je moet gebruiken ipv list() om elementen samen te nemen.
  tabPanel(
    title = tagList(icon("table"), "Datatable 1"),
    
    tags$p(
      # Hier plaatsen we icon() in een span() met extra vormgeving
      tags$span(icon("question-circle"), style = "font-size: 1.4em; color: green;"),
      "mtcars data. Selecteer een rij om in de plot te zien!"),
    
    DT::dataTableOutput("dt_mtcars_1")
  ),
  tabPanel(
    title = tagList(icon("table"), "Datatable 2"),
    
    DT::dataTableOutput("dt_mtcars_2")
  ),
  tabPanel(
    title = tagList(icon("chart-line"), "Plot"),
    
    plotOutput("plot_mtcars")  
  ),
  tabPanel(
    title = tagList(icon("bug"), "Debug"),
    
    verbatimTextOutput("txt_out")  
  )
  
  
)

server <- function(input, output, session) {
  
  output$dt_mtcars_1 <- DT::renderDataTable({
    
    datatable(mtcars,
              selection = "multiple"
    )
    
  })
  
  output$dt_mtcars_2 <- DT::renderDataTable({
    
    datatable(mtcars,
              selection = "multiple"
    )
    
  })
  
  
  dt_rows_selected_1 <- reactive({
    input$dt_mtcars_1_rows_selected
  })
  
  dt_rows_selected_2 <- reactive({
    input$dt_mtcars_2_rows_selected
  })
  
  
  output$plot_mtcars <- renderPlot({
    
    
    
    p <- ggplot(mtcars, aes(x=wt, y=disp)) +
      geom_point() + 
      theme_minimal(base_size=14)
    
    i_rows_1 <- dt_rows_selected_1()
    if(!is.null(i_rows_1)){
      
      p <- p +
        geom_point(data = mtcars[i_rows_1,],
                   aes(color = "red", size = 3))
      
    }
    
    i_rows_2 <- dt_rows_selected_2()
    if(!is.null(i_rows_2)){
      
      p <- p +
        geom_point(data = mtcars[i_rows_2,],
                   aes(color = "blue", size = 3))
      
    }
    
    p
    
  })
  
  
  output$txt_out <- renderPrint({
    reactiveValuesToList(input)
  })
}

shinyApp(ui, server)

