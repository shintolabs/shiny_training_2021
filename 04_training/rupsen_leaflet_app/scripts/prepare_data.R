
library(sf)
library(leaflet)
library(dplyr)

try(setwd("04_training/rupsen0/"), silent = TRUE)


buurten <- st_read("data/buurten/buurten.shp") %>%
  st_transform(4326)


rupsen <- st_read("data/meldingen/meldingen-openbare-ruimte.shp")


plot_rupsen_data <- function(){
  
  leaflet() %>%
    addTiles() %>%
    addCircleMarkers(data = rupsen)

}

plot_buurten_data <- function(){
  leaflet() %>%
    addTiles() %>%
    addPolygons(data = buurten, stroke = TRUE, weight = 1, col = "#fff",
                fill = TRUE, fillColor = "red")
  
}



buurt_rupsen <- st_join(buurten, rupsen)

# let op: sf is heel langzaam!
buurt_rupsen_tab <- dplyr::count(buurt_rupsen, buurtnaam)



# Data exporteren
saveRDS(buurten, "data/processed/buurten.rds")
saveRDS(rupsen, "data/processed/rupsen.rds")
saveRDS(buurt_rupsen_tab, "data/processed/buurt_rupsen_tab.rds")

#
#sf::st_write

