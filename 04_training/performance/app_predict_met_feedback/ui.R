
navbarPage(
  theme = bs_theme(version = 4, bootswatch = "flatly"),
  title = "Predict app",
  
  useShinyjs(),
  includeCSS("www/style.css"),
  useToastr(),
  #use_busy_bar(color = "#01DF01", height = "15px"),
  
  tabPanel(
    title = tagList(icon("home"), "Home"),
    
    fluidPage(
      fluidRow(
        column(6,
               
               cardBox(
                 title = "Model instellingen",
                 subtitle = "Kies hier variabelen voor je model",
                 
                 selectInput("sel_dataset", "Dataset", choices = c("mtcars","iris"), width = "100%"),
                 selectInput("sel_yvar", "Response variabele", choices = NULL, width = "100%"),
                 selectizeInput("sel_xvar", "Predictors", 
                             choices = NULL, multiple = TRUE, width = "100%",
                             options = list(plugins = list("remove_button"))),
                 
                 shinyjs::disabled(
                   actionButton("btn_fit_model", "Fit model", icon = icon("play"),
                                class = "btn-success btn-lg")  
                 ),
                 
                 #tags$br(),
                 #tags$div(style = "margin: 20px;",
                 actionButton("btn_reset", "Reset", icon = icon("sync"), class = "btn-light")  
                 #)
               )
               
        ),
        column(6,
               
               shinyjs::hidden(
                 tags$div(id = "box_samenvatting",
                    
                          
                    cardBox(
                      title = "Samenvatting",
                      subtitle = "Goodness of fit",
                      uiOutput("ui_model_summary"),
                      tags$hr(),
                      tags$h6("Variable importance", class = "card-subtitle"),
                      withSpinner(
                        plotOutput("plot_model_importance", height = "300px")
                      )
                    )
                    
                 )
               )

        )
      ),
      fluidRow(
        column(6,
               
               shinyjs::hidden(
                 tags$div(id = "box_voorspelling_1",
                          
                  cardBox(
                    title = "Voorspellingen",
                    subtitle = "Kies een predictor",
                    withSpinner(
                      plotOutput("plot_pred_1") 
                    ),
                     
                     selectizeInput("sel_predictor_1", NULL, choices = NULL)
                  )
                 )
               )
               
        ),
        column(6,
               
               shinyjs::hidden(
                 tags$div(id = "box_voorspelling_2",
                  cardBox(
                    title = "Voorspellingen",
                    subtitle = "Kies een predictor",
                     withSpinner(
                       plotOutput("plot_pred_2")
                     ),
                     selectizeInput("sel_predictor_2", NULL, choices = NULL)
                  )
                 )
               )
               
        )
      )
    )
    
  ),
  tabPanel(
    title = tagList(icon("table"), "Data"),
    
    fluidRow(
      column(8, 
             cardBox(
               title = "Data",
               subtitle = "Eerste kolom is de response, daarna predictors, dan de fit",
              DT::dataTableOutput("dt_model_data")
             )
      )
    )
    
  )
  
  
)
