
function(input, output, session){
  
  # Omdat input$sel_dataset een character is ("mtcars") moeten we de bijbehorende
  # dataset 'ophalen' met get()
  model_data <- reactive({
    base::get(input$sel_dataset)
  })
  
  model_data_columns <- reactive({
    names(model_data())
  })
  
  data_predictor_choices <- reactive({
    setdiff(model_data_columns(), input$sel_yvar)
  })
  
  # Als een dataset gekozen, stel de mogelijke response variabelen in
  observeEvent(input$sel_dataset, {
    
    kols <- model_data_columns()
    updateSelectInput(session, "sel_yvar", choices = kols)
    
  })
  
  # Als een response gekozen, stel de mogelijke predictor variabelen in
  observeEvent(input$sel_yvar, {
    
    kol_choices <- data_predictor_choices()
    updateSelectInput(session, "sel_xvar", choices = kol_choices)
    
  })
  
  # Knopje enable/disable als er een gefit model is.
  observe({
    
    x_sel <- input$sel_xvar
    
    if(is.null(x_sel)){
      shinyjs::disable("btn_fit_model")
    } else {
      shinyjs::enable("btn_fit_model")
    }
    
  })
  
  # Panels aan/uitzetten als er wel/geen gefit model is
  observe({
    
    mod <- fitted_model()
    
    if(is.null(mod)){
      shinyjs::hide("box_samenvatting")
      shinyjs::hide("box_voorspelling_1")
      shinyjs::hide("box_voorspelling_2")
    } else {
      shinyjs::show("box_samenvatting")
      shinyjs::show("box_voorspelling_1")
      shinyjs::show("box_voorspelling_2")
    }
    
  })
  
  # Model fitten.
  # Eerst lege 'containers' aanmaken waar we het gefitte model en de gebruike variabelen
  # gaan bewaren.
  fitted_model <- reactiveVal()
  last_predictors <- reactiveVal()
  last_response <- reactiveVal()
  
  
  # Bij een actionButton hoort (bijna) altijd een observeEvent.
  observeEvent(input$btn_fit_model, {
    
    
    toastr_info("Model is being fit, please be patient")
    
    fit <- fit_myrandomforest(yvar = input$sel_yvar,
                              xvar = input$sel_xvar,
                              data = model_data())
    Sys.sleep(5)
    
    # sla waarde op in de reactiveVal
    fitted_model(fit)
    
    # om de waarde uit de reactiveVal te lezen doe je,
    # fitted_model()
    
    # om de reactiveVal te legen:
    # fitted_model(NULL)
    
    # Sla gebruikte variabelen op.
    last_predictors(input$sel_xvar)
    last_response(input$sel_yvar)
    
    toastr_success("Model fit complete!")
    
  })
  
  # Maak een dataset met de predictors, de response, en de predicted values.
  last_model_data <- reactive({
    
    req(last_predictors())
    
    out <- model_data()[,c(last_response(), last_predictors())]
    out$predicted <- model_predictions()
    
    out
  })
  
  # Meest eenvoudige datatable, maar zie:
  # https://rstudio.github.io/DT/010-style.html
  # Om bv. kolommen of cellen in te kleuren. Er zijn veel opties met datatable!
  output$dt_model_data <- DT::renderDataTable({
    last_model_data() %>%
      datatable()
  })
  
  
  # Reset knop: we hoeven alleen onze reactiveVal's te legen.
  # Omdat veel content het fitted_model() nodig heeft (vandaar verder hieronder: req(fitted_model()))
  # wordt die content vanzelf leeg als we hier fitted_model() op NULL zetten.
  observeEvent(input$btn_reset, {
    
    fitted_model(NULL)
    last_predictors(NULL)
    last_response(NULL)
    
  })
  
  # Voorspellingen (vector)
  model_predictions <- reactive({
    
    # Als is.null(fitted_model()), stop de reactive dan met een onzichtbare foutmelding
    req(fitted_model())
    
    predict(fitted_model())
  })
  
  # De response variabele (vector)
  model_response <- reactive({
    model_data()[,input$sel_yvar]
  })
  
  # De RMSE van de fit. Elke keer dat we een model fitten,
  # wordt deze automatisch opnieuw berekend.
  model_fit_rmse <- reactive({
    
    pred <- model_predictions()
    meas <- model_response()
    
    sqrt(mean((meas - pred)^2))
  })
  
  # Idem voor de R2
  model_fit_r2 <- reactive({
    
    1 - model_fit_rmse() / sd(model_response())
    
  })
  
  #------ Samenvatting ------
  # uitOutput <--> renderUI
  # renderUI maakt HTML aan, die in de uitOutput() wordt geplaatst
  # in de UI. 
  output$ui_model_summary <- renderUI({
    
    rmse <- model_fit_rmse()
    r2 <- model_fit_r2()
    
    # De vormgeving van RMSE en R2 heb ik nog verbeterd.
    # --> Probeer zelf een functie te maken die zo'n boxje aanmaakt (dus dat je de functie
    # twee keer aanroept, een keer met label RMSE en waarde round(rmse,2), en een keer voor R2).
    tags$div(
      style = "width: 50%",
      
      tags$div(style = "margin: 8px; border: 1px solid lightgrey; border-radius: 4px; padding: 7px;",
        tags$span("RMSE", style = "font-size: 1.2em;"),
        tags$span(round(rmse,2), style = "font-size: 1.4em; font-weight: 700; float: right;")
      ),
      tags$div(style = "margin: 8px; border: 1px solid lightgrey; border-radius: 4px; padding: 7px;",
        tags$span(HTML("R<sup>2</sup>"), style = "font-size: 1.2em;"),
        tags$span(round(100*r2,2), style = "font-size: 1.4em; font-weight: 700; float: right;")
      )
      
    )

  })
  
  # Maak de model importance plot, gebruik hiervoor een functie die een gefit model als input neemt.
  output$plot_model_importance <- renderPlot({
    req(fitted_model())
    Sys.sleep(1)
    plot_model_importance_top3(fitted_model())
  })
  
  # Hier vullen we de keuzes voor de selectinput onder elk voorspel plotje.
  observeEvent(last_predictors(), {
    
    updateSelectInput(session, "sel_predictor_1", choices = last_predictors())
    updateSelectInput(session, "sel_predictor_2", choices = last_predictors())
    
  })
  
  # Eerste plot
  output$plot_pred_1 <- renderPlot({
    
    y <- model_predictions()
    x_var <- input$sel_predictor_1
    
    req(y)
    req(x_var)
    Sys.sleep(1)
    x <- model_data()[,x_var]
    
    data.frame(x = x, prediction = y) %>%
      ggplot(aes(x = x, y = prediction)) +
      geom_point() +
      geom_smooth() +
      theme_minimal()
    
  })
  
  # Tweede plot. Er is veel herhaling van code met plot1 : dat gaan we met modules nog verbeteren.
  output$plot_pred_2 <- renderPlot({
    
    y <- model_predictions()
    x_var <- input$sel_predictor_2
    
    req(y)
    req(x_var)
    Sys.sleep(1)
    x <- model_data()[,x_var]
    
    data.frame(x = x, prediction = y) %>%
      ggplot(aes(x = x, y = prediction)) +
      geom_point() +
      geom_smooth() +
      theme_minimal()
    
  })
  
  
  
  
}

