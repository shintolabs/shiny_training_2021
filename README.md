# Shiny Training - 2021

Op deze pagina kun je alle materialen voor de training 'R/Shiny' (Okt.-Nov. 2021) downloaden.



## Video's

De opnames van de trainingen zijn beschikbaar op youtube, op een private channel. Je kunt dus alleen de video bekijken via deze links:

[R/Shiny Training - Deel 1](https://youtu.be/38ArlE-MrKg)

[R/Shiny Training - Deel 2](https://youtu.be/Hm1X4SxEKIE)

[R/Shiny Training - Deel 3](https://youtu.be/S4qnAW603ew)

[R/Shiny Training - Deel 4](https://youtu.be/W7CwjRoRQvQ)





## Resources

Een verzameling belangrijke web resources, om shiny te leren en als naslagwerk tijdens ontwikkelwerk.


- [Mastering Shiny](https://mastering-shiny.org/index.html). Boek over Shiny door Hadley Wickham (Rstudio), in progress maar veel zeer nuttige content. 

- [Rstudio Shiny Articles](https://shiny.rstudio.com/articles/). Alle aspecten van een shiny app op een rijtje. Verplicht materiaal.

- [Awesome Shiny Extensions](https://github.com/nanxstats/awesome-shiny-extensions). Lange lijst van (alle?) packages die je kunt gebruiken om shiny apps uit te breiden.

- [FontAwesome Icons](https://fontawesome.com/v5.15/icons?d=gallery&p=2&m=free). Lijst van icons die je snel in shiny apps kunt gebruiken, via `icon("iconname")`.

- [HTML Tutorials](https://www.w3schools.com/html/). HTML tutorial en naslagwerk van W3Schools, zeer toegankelijk materiaal.

- [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/). Technische beschrijving van Bootstrap, het CSS framework dat we gebruiken in shiny apps. Elementen die hier zijn beschreven kun je snel in shiny apps gebruiken.

- [A Learning Guide to R](https://remkoduursma.github.io/learningguidetor/). Inleidend en meer geavanceerd boek over R, inc. 'version control with git', 'project management and workflow', en 'functions lists and loops'. 
